'use strict';

const Alexa = require('alexa-sdk');
const Duration = require('iso8601-duration');
const parse = Duration.parse;
const toSeconds = Duration.toSeconds;

const handlers = {
  'Meditate': function() {
    //TODO take any time period? https://developer.amazon.com/docs/custom-skills/slot-type-reference.html#duration=


    const duration = this.event.request.intent.slots.duration.value;
    let seconds;
    if (duration != null) {
      seconds = toSeconds(parse(duration));
    }

    const speechOutput = seconds ? `beginning meditation for ${seconds} seconds` : 'beginning meditation';
    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/sound_1.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddFire': function() {
    const speechOutput = "Adding fire";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/fire.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddRain': function() {
    const speechOutput = "Adding rain";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/rain%2Bfire.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddThunder': function() {
    const speechOutput = "Adding thunder";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/rain%2Bthunder.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddWind': function() {
    const speechOutput = "Adding wind";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/waves%2Bwind.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddWaves': function() {
    const speechOutput = "Adding waves";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/waves.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddCrickets': function() {
    const speechOutput = "Adding crickets";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/crickets.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddBirds': function() {
    const speechOutput = "Adding birds";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/crickets%2Bbirds.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'AddWhiteNoise': function() {
    const speechOutput = "Adding white noise";

    const behavior = 'REPLACE_ALL';
    const url = 'https://s3.amazonaws.com/meditate-skill-sounds/crickets%2Bbirds%2Bwnoise.mp3';
    const token = 'meditateToken';
    const expectedPreviousToken = null;
    const offsetInMilliseconds = 0;

    this.response.speak(speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },
  'ClearSounds': function() {
    this.response.speak("Clearing sounds").audioPlayerStop();
    this.emit(':responseReady');
  },
  'StopMeditation': function() {
    console.log("Playback stopped");
    this.response.speak("ending meditation").audioPlayerStop();
    //do not return a response, as per https://developer.amazon.com/docs/custom-skills/audioplayer-interface-reference.html#playbackstopped
    this.emit(':responseReady');
  },
  'PlaybackStarted' : function () {
    /*
     * AudioPlayer.PlaybackStarted Directive received.
     * Confirming that requested audio file began playing.
     * Do not send any specific response.
     */
    console.log("Playback started");
    this.emit(':responseReady');
  },
  'PlaybackFinished' : function () {
    /*
     * AudioPlayer.PlaybackFinished Directive received.
     * Confirming that audio file completed playing.
     * Do not send any specific response.
     */
    console.log("Playback finished");
    this.emit(':responseReady');
  },
  'PlaybackStopped' : function () {
    /*
     * AudioPlayer.PlaybackStopped Directive received.
     * Confirming that audio file stopped playing.
     */
    console.log("Playback stopped");
    this.response.speak("ending meditation").audioPlayerStop();

    //do not return a response, as per https://developer.amazon.com/docs/custom-skills/audioplayer-interface-reference.html#playbackstopped
    this.emit(':responseReady');
  },
  'PlaybackNearlyFinished' : function () {
    /*
     * AudioPlayer.PlaybackNearlyFinished Directive received.
     * Replacing queue with the URL again.
     * This should not happen on live streams
     */
    console.log("Playback nearly finished");
    // controller.playLater.call(this, audioData(this.event.request).url);
  },
  'PlaybackFailed' : function () {
    /*
     * AudioPlayer.PlaybackFailed Directive received.
     * Logging the error and stop playing.
     *
     *  TODO : should we enqueue another URL ?  The risk is to loop if the problem comes from the stream itself.
     *         Smart skills should try to enqueue the URL 2-3 times and if the error still occurs,
     *         decide to play an MP3 with an error message instead.
     *         These error should be reported.  We recommend to create an Alert on Cloudwatch to send an email based on volume.
     */
    console.log("Playback Failed : %j", this.event.request.error);
    // controller.clear.call(this);
  },
  'Unhandled': function() {
    const message = 'Welcome to soundscape, where you can create a sonic atmosphere to your liking. You can say things like "Alexa, tell soundscape to add fire."';
    this.response.speak(message);
    this.emit(':responseReady');
  }
};

module.exports.luckyNumber = (event, context, callback) => {
  const alexa = Alexa.handler(event, context, callback);
  alexa.appId = 'amzn1.ask.skill.8b406fb2-892d-40ab-9a83-f1a597e2ee5e';
  alexa.registerHandlers(handlers);
  alexa.execute();

  // const upperLimit = event.request.intent.slots.UpperLimit.value || 100;
  // const number = getRandomInt(0, upperLimit);
  // const response = {
  //   version: '1.0',
  //   response: {
  //     outputSpeech: {
  //       type: 'PlainText',
  //       text: `Your lucky number is ${number}`,
  //     },
  //     shouldEndSession: false,
  //   },
  // };

  //callback(null, response);
};